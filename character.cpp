#include "character.h"
#include <QPainter>
#include "gamestate.h"
#include"oil_catcher.h"
#include "right_fail_cather.h"
#include "left_fail_catcher.h"


Character::Character(GameState* _gs,QObject *parent) : Drawable(_gs,parent)
{
    drop_down_timer.setInterval(300);
    drop_down_timer.setSingleShot(false);
    connect(&drop_down_timer,SIGNAL(timeout()),this,SLOT(Drop_step()));
    drop_down_timer.start();
}

bool Character::FillCup()
{
    if(cup_fill_level==3)
        return false;
    cup_fill_level++;
    gs->AddGoals(1);
    return true;
}

void Character::Drop_step()
{

    if(drop_down_step == 3){
        drop_down_timer.stop();
        drop_down_step = 1;
        is_Drop_Down = false;
        return;
    }
    drop_down_step++;
}

void Character::draw(QPaintDevice *painter)
{
    static QImage Character_Up_1(":/image/oil_tiles/Character_Up_1.png");
    static QImage Character_Up_2(":/image/oil_tiles/Character_Up_2.png");
    static QImage Character_Up_3(":/image/oil_tiles/Character_Up_3.png");
    static QImage Character_Up_4(":/image/oil_tiles/Character_Up_4.png");
    static QImage Character_Up_5(":/image/oil_tiles/Character_Up_5.png");
    static QImage Cup_Fill_11(":/image/oil_tiles/Cup_Fill_11");
    static QImage Cup_Fill_21(":/image/oil_tiles/Cup_Fill_21");
    static QImage Cup_Fill_31(":/image/oil_tiles/Cup_Fill_31");
    static QImage Cup_Fill_12(":/image/oil_tiles/Cup_Fill_12");
    static QImage Cup_Fill_22(":/image/oil_tiles/Cup_Fill_22");
    static QImage Cup_Fill_32(":/image/oil_tiles/Cup_Fill_32");
    static QImage Cup_Fill_13(":/image/oil_tiles/Cup_Fill_13");
    static QImage Cup_Fill_23(":/image/oil_tiles/Cup_Fill_23");
    static QImage Cup_Fill_33(":/image/oil_tiles/Cup_Fill_33");
    static QImage Character_Down_1(":/image/oil_tiles/Character_Down_1.png");
    static QImage Character_Down_2(":/image/oil_tiles/Character_Down_2.png");
    static QImage Character_Down_1_Cup_Up(":/image/oil_tiles/Character_Down_1_Cup_Up.png");
    static QImage Character_Down_2_Cup_Up(":/image/oil_tiles/Character_Down_2_Cup_Up.png");
    static QImage Character_Down_1_Cup_Down(":/image/oil_tiles/Character_Down_1_Cup_Down.png");
    static QImage Character_Down_1_Cup_Down_Tear(":/image/oil_tiles/Character_Down_1_Cup_Down_Tear.png");
    static QImage Character_Down_1_Cup_Down_Tear_Blow(":/image/oil_tiles/Character_Down_1_Cup_Down_Tear_Blow.png");
    static QImage Character_Down_2_Cup_Down(":/image/oil_tiles/Character_Down_2_Cup_Down.png");
    static QImage Character_Down_2_Cup_Down_Tear(":/image/oil_tiles/Character_Down_2_Cup_Down_Tear.png");
    static QImage Character_Down_2_Cup_Down_Tear_Blow(":/image/oil_tiles/Character_Down_2_Cup_Down_Tear_Blow.png");




    QPainter p(painter);
    switch(character_step){
    case 1:p.drawImage(0,47,Character_Up_1,255,0);
        p.drawImage(0,47,Character_Down_1,255,0);
        if(!is_Drop_Down)p.drawImage(0,47,Character_Down_1_Cup_Up,255,0);
        break;
    case 2:p.drawImage(0,47,Character_Up_2,255,0);break;
    case 3:p.drawImage(0,47,Character_Up_3,255,0);break;
    case 4:p.drawImage(0,47,Character_Up_4,255,0);break;
    case 5:p.drawImage(0,47,Character_Up_5,255,0);
        p.drawImage(0,47,Character_Down_2,255,0);
        if(!is_Drop_Down)p.drawImage(0,47,Character_Down_2_Cup_Up,255,0);
        break;
    }
    switch(cup_fill_level){
    case 3:{
        switch(character_step){
        case 2:p.drawImage(0,47,Cup_Fill_11,255,0);break;
        case 3:p.drawImage(0,47,Cup_Fill_21,255,0);break;
        case 4:p.drawImage(0,47,Cup_Fill_31,255,0);break;
        }}
    case 2:{
        switch(character_step){
        case 2:p.drawImage(0,47,Cup_Fill_12,255,0);break;
        case 3:p.drawImage(0,47,Cup_Fill_22,255,0);break;
        case 4:p.drawImage(0,47,Cup_Fill_32,255,0);break;
        }}
    case 1:{
        switch(character_step){
        case 2:p.drawImage(0,47,Cup_Fill_13,255,0);break;
        case 3:p.drawImage(0,47,Cup_Fill_23,255,0);break;
        case 4:p.drawImage(0,47,Cup_Fill_33,255,0);break;
        }}


    }
    if(is_Drop_Down){
        switch(drop_down_step){
        case 1:if(is_Right_Drop)p.drawImage(0,47,Character_Down_2_Cup_Down,255,0);
            else p.drawImage(0,47,Character_Down_1_Cup_Down,255,0);
            break;
        case 2:if(is_Right_Drop)p.drawImage(0,47,Character_Down_2_Cup_Down_Tear,255,0);
            else p.drawImage(0,47,Character_Down_1_Cup_Down_Tear,255,0);
            break;
        case 3:if(!is_Fail_Drop){if(is_Right_Drop){p.drawImage(0,47,Character_Down_2_Cup_Down_Tear_Blow,255,0);
                    gs->AddGoals(tmp_fill_level);tmp_fill_level=0;}
            else{ p.drawImage(0,47,Character_Down_1_Cup_Down_Tear_Blow,255,0);gs->AddGoals(tmp_fill_level);tmp_fill_level=0;}
            }else{
                if(is_Right_Drop){gs->getRight_Fail_Catcher()->Do_Wet();}
                            else {gs->getLeft_Fail_Catcher()->Do_Wet();}
            }
            break;
        }
    }


}

void Character::MoveRight()
{
    if(character_step==5){
        if(cup_fill_level==0)return;
        is_Right_Drop = true;
        is_Drop_Down = true;
        is_Fail_Drop = true;
        if(gs->getOil_Catcher()->getStep() == 4 && character_step == 5){
            is_Fail_Drop = false;
        }
        tmp_fill_level = cup_fill_level;
        cup_fill_level = 0;
        drop_down_timer.start();
    }
    if(character_step!=5){
        character_step++;
    }


}

void Character::MoveLeft()
{
    if(character_step==1){
        if(cup_fill_level==0)return;
        is_Right_Drop = false;
        is_Drop_Down = true;
        is_Fail_Drop = true;
        if(gs->getOil_Catcher()->getStep() == 1 && character_step == 1) {
                is_Fail_Drop = false;
        }
         tmp_fill_level = cup_fill_level;
        cup_fill_level = 0;
        drop_down_timer.start();
    }
    if(character_step!=1){
        character_step--;
    }

}
