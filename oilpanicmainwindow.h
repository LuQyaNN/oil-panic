#ifndef OILPANICMAINWINDOW_H
#define OILPANICMAINWINDOW_H

#include <QMainWindow>

class OilPanicMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit OilPanicMainWindow(QWidget *parent = 0);
QSize minimumSizeHint()const{return QSize(255,386);}
signals:
private:
    class Oil_Panic_Widget* opw;
    class GameState* gs;

private slots:
    void keyPressEvent(QKeyEvent * ke) override;
    void OnLeft();
    void OnRight();
    void OnPause();


public slots:
};

#endif // OILPANICMAINWINDOW_H
