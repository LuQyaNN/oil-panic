#ifndef UP_FAIL_H
#define UP_FAIL_H
#include "drawable.h"

class Up_Fail : public Drawable
{
    Q_OBJECT
public:
    Up_Fail(class GameState* _gs, int _fail_col);
     virtual void draw( class QPaintDevice *painter)override;

private:
    int fail_col;
    QTimer step_timer;
    int step = 1;
private slots:
     void DoStep();
};

#endif // UP_FAIL_H
