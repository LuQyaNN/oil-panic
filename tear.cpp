#include "tear.h"
#include <QPainter>
#include "gamestate.h"
#include "character.h"
#include "up_fail.h"

Tear::Tear(GameState *game_state,stance_column stance_c):Drawable(game_state),stance_col(stance_c)
{

    t.setSingleShot(false);
    t.setInterval(1000);
    connect(&t,SIGNAL(timeout()),this,SLOT(step()));
    t.start();
}


void Tear::draw(QPaintDevice *painter)
{
    static QImage tear11(":/image/oil_tiles/tear11.png");
    static QImage tear12(":/image/oil_tiles/tear12.png");
    static QImage tear13(":/image/oil_tiles/tear13.png");
    static QImage tear14(":/image/oil_tiles/tear14.png");
    static QImage tear21(":/image/oil_tiles/tear21.png");
    static QImage tear22(":/image/oil_tiles/tear22.png");
    static QImage tear23(":/image/oil_tiles/tear23.png");
    static QImage tear24(":/image/oil_tiles/tear24.png");
    static QImage tear31(":/image/oil_tiles/tear31.png");
    static QImage tear32(":/image/oil_tiles/tear32.png");
    static QImage tear33(":/image/oil_tiles/tear33.png");
    static QImage tear34(":/image/oil_tiles/tear34.png");

    QPainter p(painter);
    switch(stance_col){

    case 1:{switch(stance_row){
            case 1:p.drawImage(0,47,tear11,255,0);break;
            case 2:p.drawImage(0,47,tear12,255,0);break;
            case 3:p.drawImage(0,47,tear13,255,0);break;
            case 4:p.drawImage(0,47,tear14,255,0);break;
        }}break;
    case 2:{switch(stance_row){
            case 1:p.drawImage(0,47,tear21,255,0);break;
            case 2:p.drawImage(0,47,tear22,255,0);break;
            case 3:p.drawImage(0,47,tear23,255,0);break;
            case 4:p.drawImage(0,47,tear24,255,0);break;
        }}break;
    case 3:{switch(stance_row){
            case 1:p.drawImage(0,47,tear31,255,0);break;
            case 2:p.drawImage(0,47,tear32,255,0);break;
            case 3:p.drawImage(0,47,tear33,255,0);break;
            case 4:p.drawImage(0,47,tear34,255,0);break;
        }}break;


    }
}

Tear::~Tear()
{

    gs->getTears().remove(static_cast<Tear::stance_column>(stance_col));
}


void Tear::step()
{
    if(stance_row!=4)
        stance_row++;
    else{
        deleteLater();
        if(gs->getCharacter()->getStep()== stance_col+1){
            gs->getCharacter()->FillCup();
        }
        else{
            gs->getUpFails()[stance_col-1] = new Up_Fail(gs,stance_col);
            gs->AddUp_fails_count(1);
        }
    }
}
