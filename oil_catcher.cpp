#include "oil_catcher.h"
#include "gamestate.h"
#include <QPainter>

Oil_Catcher::Oil_Catcher(GameState* _gs, QObject *parent):Drawable(_gs,parent)
{
t.setInterval(1000);
t.setSingleShot(false);
connect(&t,SIGNAL(timeout()),this,SLOT(Do_Step()));
t.start();
}

void Oil_Catcher::draw(QPaintDevice *painter)
{
    static QImage Catcher_1(":/image/oil_tiles/Catcher_1.png");
    static QImage Catcher_2(":/image/oil_tiles/Catcher_2.png");
    static QImage Catcher_3(":/image/oil_tiles/Catcher_3.png");
    static QImage Catcher_4(":/image/oil_tiles/Catcher_4.png");
QPainter p(painter);
switch(step){
case 1:p.drawImage(0,47,Catcher_1,255,0);break;
    case 2:p.drawImage(0,47,Catcher_2,255,0);break;
    case 3:p.drawImage(0,47,Catcher_3,255,0);break;
    case 4:p.drawImage(0,47,Catcher_4,255,0);break;
}
}

void Oil_Catcher::Do_Step()
{
    if(back){
        step--;
    }else{
        step++;
    }
    if(step!=4 || step!=1)
        t.setInterval(1000);
    if(step==4){

        back = true;
        t.setInterval(3000);
    }
    if(step == 1){
        back = false;
        t.setInterval(3000);
    }

}
