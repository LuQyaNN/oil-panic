#include "up_fail.h"
#include <QPainter>
#include "gamestate.h"

Up_Fail::Up_Fail(GameState *_gs, int _fail_col):Drawable(_gs),fail_col(_fail_col)
{

step_timer.setInterval(500);
step_timer.setSingleShot(false);
connect(&step_timer,SIGNAL(timeout()),this,SLOT(DoStep()));
step_timer.start();
}

void Up_Fail::draw(QPaintDevice *painter)
{
    static QImage Bulk_11(":/image/oil_tiles/Bulk_11.png");
    static QImage Bulk_12(":/image/oil_tiles/Bulk_12.png");
    static QImage Bulk_13(":/image/oil_tiles/Bulk_13.png");
    static QImage Bulk_21(":/image/oil_tiles/Bulk_21.png");
    static QImage Bulk_22(":/image/oil_tiles/Bulk_22.png");
    static QImage Bulk_23(":/image/oil_tiles/Bulk_23.png");
    static QImage Bulk_31(":/image/oil_tiles/Bulk_31.png");
    static QImage Bulk_32(":/image/oil_tiles/Bulk_32.png");
    static QImage Bulk_33(":/image/oil_tiles/Bulk_33.png");
    QPainter p(painter);
    switch(fail_col){
    case 1:{switch(step%4){
        case 3:{p.drawImage(0,47,Bulk_11,255,0);}
        case 2:{p.drawImage(0,47,Bulk_12,255,0);}
        case 1:{p.drawImage(0,47,Bulk_13,255,0);}

        }}break;
        case 2:{switch(step%4){
        case 3:{p.drawImage(0,47,Bulk_21,255,0);}
        case 2:{p.drawImage(0,47,Bulk_22,255,0);}
        case 1:{p.drawImage(0,47,Bulk_23,255,0);}

        }}break;
        case 3:{switch(step%4){
        case 3:{p.drawImage(0,47,Bulk_31,255,0);}
        case 2:{p.drawImage(0,47,Bulk_32,255,0);}
        case 1:{p.drawImage(0,47,Bulk_33,255,0);}
        }}break;
    }
}

void Up_Fail::DoStep()
{
    step++;
    if(step == 9){
        gs->getUpFails()[fail_col-1]=0;
        deleteLater();
    }
}
