#ifndef TEAR_H
#define TEAR_H
#include "drawable.h"

class Tear : public Drawable
{
    Q_OBJECT
public:
    enum stance_column{first=1,second,threeth};
    Tear(class GameState* game_state,stance_column stance_c);
    void draw(QPaintDevice *painter);
    ~Tear();
private slots:
    void step();
private:

     int stance_row = 1;
    int stance_col;
    QTimer t;
};

#endif // TEAR_H
