#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <QObject>
#include <QTimer>

class Drawable : public QObject
{
    Q_OBJECT
public:
    explicit Drawable(class GameState *_gs, QObject *parent = 0);
 virtual void draw(class QPaintDevice *painter){};
protected:
    class GameState* gs;
signals:

public slots:
};

#endif // DRAWABLE_H
