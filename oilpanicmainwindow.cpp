#include "oilpanicmainwindow.h"
#include <QKeyEvent>
#include "oil_panic_widget.h"
#include <QPainter>
#include "gamestate.h"
#include "character.h"

OilPanicMainWindow::OilPanicMainWindow(QWidget *parent) : QMainWindow(parent)
{
    gs = new GameState;
    opw = new Oil_Panic_Widget(gs,this);
    resize(255,386);


}

void OilPanicMainWindow::keyPressEvent(QKeyEvent * ke) {

    switch(ke->key()){
           case Qt::Key::Key_A:{OnLeft();break;}
    case Qt::Key::Key_D:{OnRight();break;}
        case Qt::Key::Key_P:{OnPause();break;}
    }
}

void OilPanicMainWindow::OnLeft()
{
gs->getCharacter()->MoveLeft();
}

void OilPanicMainWindow::OnRight()
{
gs->getCharacter()->MoveRight();
}

void OilPanicMainWindow::OnPause()
{

}


