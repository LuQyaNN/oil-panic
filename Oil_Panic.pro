QT += core widgets


CONFIG += c++11

TARGET = Oil_Panic
CONFIG += console

TEMPLATE = app

SOURCES += main.cpp \
    oilpanicmainwindow.cpp \
    oil_panic_widget.cpp \
    character.cpp \
    gamestate.cpp \
    drawable.cpp \
    tear.cpp \
    up_fail.cpp \
    oil_catcher.cpp \
    right_fail_cather.cpp \
    left_fail_catcher.cpp

HEADERS += \
    oilpanicmainwindow.h \
    oil_panic_widget.h \
    character.h \
    gamestate.h \
    drawable.h \
    tear.h \
    up_fail.h \
    oil_catcher.h \
    right_fail_cather.h \
    left_fail_catcher.h

RESOURCES += \
    res.qrc



