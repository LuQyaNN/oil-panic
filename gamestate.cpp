#include "gamestate.h"
#include "character.h"
#include "tear.h"
#include "oil_catcher.h"
#include<QTime>
#include <QPainter>
#include "right_fail_cather.h"
#include "left_fail_catcher.h"



GameState::GameState(QObject *parent) : Drawable(this,parent)
{
    rfc = new Right_Fail_Catcher(this);
    lfc = new Left_Fail_Catcher(this);
    ch = new Character(this);
    oc = new Oil_Catcher(this);
    up_fails.resize(3);

    tear_timer.setSingleShot(false);
    tear_timer.setInterval(2000);
    connect(&tear_timer,SIGNAL(timeout()),this,SLOT(event()));
    tear_timer.start();

}

void GameState::draw(QPaintDevice *painter)
{
    static QImage tile(":/image/tile.png");
    static QImage Miss_UP_1(":/image/oil_tiles/Miss_UP_1.png");
    static QImage Miss_UP_2(":/image/oil_tiles/Miss_UP_2.png");
    static QImage Miss_UP_3(":/image/oil_tiles/Miss_UP_3.png");
    static QImage Miss_UP(":/image/oil_tiles/Miss_UP.png");
    static QImage Miss_Down_1(":/image/oil_tiles/Miss_Down_1.png");
    static QImage Miss_Down_2(":/image/oil_tiles/Miss_Down_2.png");
    static QImage Miss_Down_3(":/image/oil_tiles/Miss_Down_3.png");
    static QImage Miss_Down(":/image/oil_tiles/Miss_Down.png");
    QPainter p(painter);
    if(up_fails_count == 3 || down_fails_count ==3){
        p.setPen(Qt::black);
        p.setFont(QFont("Arial", 30));
        p.drawText(0,0,255,386, Qt::AlignCenter, "Game Over");

        return;
    }


    p.drawImage(0,0, tile,0,0,255,386);
    oc->draw(painter);
    ch->draw(painter);
    rfc->draw(painter);
    lfc->draw(painter);
    for(auto& tear:tears){
        tear->draw(painter);
    }
    for(auto& fail:up_fails){
        if(fail!=0)fail->draw(painter);
    }
    QPainter p2(painter);
    switch(up_fails_count){
    case 3:{p2.drawImage(0,47, Miss_UP_3,255,0);}
    case 2:{p2.drawImage(0,47, Miss_UP_2,255,0);}
    case 1:{p2.drawImage(0,47, Miss_UP_1,255,0); p2.drawImage(0,47, Miss_UP,255,0);}
    }
    switch(down_fails_count){
    case 3:{p2.drawImage(0,47, Miss_Down_3,255,0);}
    case 2:{p2.drawImage(0,47, Miss_Down_2,255,0);}
    case 1:{p2.drawImage(0,47, Miss_Down_1,255,0); p2.drawImage(0,47, Miss_Down,255,0);}
    }
    p.setPen(Qt::black);
    p.setFont(QFont("Arial", 20));
    p.drawText(0,0,255,386,Qt::AlignTop ,QString::number(goals));
}

void GameState::event()
{

    qsrand(QTime::currentTime().msecsSinceStartOfDay());
    int tmp= 1+qrand()%3;
    if(tears.find(static_cast<Tear::stance_column>(tmp)) == tears.end())
        tears.insert(static_cast<Tear::stance_column>(tmp),new Tear(this,static_cast<Tear::stance_column>(tmp)));

}
