#ifndef OIL_PANIC_WIDGET_H
#define OIL_PANIC_WIDGET_H

#include <QWidget>

class Oil_Panic_Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Oil_Panic_Widget(class GameState* gs,QWidget *parent = 0);
    QPainter& getPainter(){return *painter;}

private:
    QPainter* painter;
    class GameState* gs;
 class QTimer* update_timer;
signals:

public slots:
protected:
    void paintEvent(QPaintEvent *event)override;
};

#endif // OIL_PANIC_WIDGET_H
