#include "right_fail_cather.h"
#include "drawable.h"
#include <QPainter>
#include "gamestate.h"
Right_Fail_Catcher::Right_Fail_Catcher(class GameState* _gs,QObject *parent ):Drawable(_gs,parent)
{
t.setInterval(500);
t.setSingleShot(false);
connect(&t,SIGNAL(timeout()),this,SLOT(Do_Step()));
t.start();
}

void Right_Fail_Catcher::draw(QPaintDevice *painter)
{
    static QImage Fail_2_Character_2(":/image/oil_tiles/Fail_2_Character_2.png");
     static QImage Fail_2_Character_3(":/image/oil_tiles/Fail_2_Character_3.png");
      static QImage Fail_2_Character_4(":/image/oil_tiles/Fail_2_Character_4.png");
      static QImage Fail_2_Character_5(":/image/oil_tiles/Fail_2_Character_5.png");
      static QImage Fail_2_Character_1(":/image/oil_tiles/Fail_2_Character_1.png");

    QPainter p(painter);
    if(is_wet){
    switch(wet_step){
    case 1:p.drawImage(0,47,Fail_2_Character_1,255,0);break;
        case 2:p.drawImage(0,47,Fail_2_Character_3,255,0);break;
    }
    }
    switch(step){

    case 1:p.drawImage(0,47,Fail_2_Character_2,255,0);break;

        case 2:p.drawImage(0,47,Fail_2_Character_4,255,0);break;
        case 3:p.drawImage(0,47,Fail_2_Character_5,255,0);break;
        case 4:p.drawImage(0,47,Fail_2_Character_4,255,0);break;
        case 5:p.drawImage(0,47,Fail_2_Character_5,255,0);break;
    }

}

void Right_Fail_Catcher::Do_Wet()
{
    is_wet = true;

}

void Right_Fail_Catcher::Do_Step()
{
     if(step==5){
         step =1;
         is_wet = false;
         wet_step = 1;
         gs->AddDown_fails_count(1);
     }


    if(is_wet){

        if(wet_step==2){
            step++;
        }else
        wet_step++;
    }

}
