#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "drawable.h"
#include <QMap>
#include "tear.h"
#include <QVector>
#include "up_fail.h"
#include <QImage>


class GameState : public Drawable
{
    Q_OBJECT
public:

    explicit GameState(QObject *parent = 0);

    void draw(QPaintDevice *painter)override;

    class Character* getCharacter(){return ch;}
    class Oil_Catcher* getOil_Catcher(){return oc;}
    class Right_Fail_Catcher* getRight_Fail_Catcher(){return rfc;}
    class Left_Fail_Catcher* getLeft_Fail_Catcher(){return lfc;}
    QMap<Tear::stance_column,Tear*>& getTears(){return tears;}
    QVector<Up_Fail*>& getUpFails(){return up_fails;}
    void AddUp_fails_count(int num){up_fails_count+=num;
                                    up_fails_count>3?up_fails_count=3:0;}
    void AddDown_fails_count(int num){down_fails_count+=num;
                                    down_fails_count>3?down_fails_count=3:0;}
    void AddGoals(int num){goals+=num;}
private:

    int up_fails_count = 0, down_fails_count = 0,goals = 0;

    class Left_Fail_Catcher* lfc;
    class Right_Fail_Catcher* rfc;
    class Oil_Catcher* oc;
    Character* ch;
    QTimer tear_timer;
    QMap<Tear::stance_column,Tear*> tears;
    QVector<Up_Fail*> up_fails;

signals:

public slots:
    void event();
};

#endif // GAMESTATE_H
