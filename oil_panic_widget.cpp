#include "oil_panic_widget.h"
#include <QPainter>
#include "gamestate.h"
#include "character.h"
#include <QTimer>


Oil_Panic_Widget::Oil_Panic_Widget(GameState *gs, QWidget *parent) : QWidget(parent)
{
resize(255,386);

this->gs = gs;
update_timer =new QTimer(this);
update_timer->setSingleShot(false);
update_timer->setInterval(100);
connect(update_timer,SIGNAL(timeout()),this,SLOT(update()));
update_timer->start();
}

void Oil_Panic_Widget::paintEvent(QPaintEvent *event)
{


    gs->draw(this);


}
