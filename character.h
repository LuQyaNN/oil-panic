#ifndef CHARACTER_H
#define CHARACTER_H

#include "drawable.h"

class Character : public Drawable
{
    Q_OBJECT
public:
    explicit Character(class GameState* _gs,QObject *parent = 0);
     int getStep(){return character_step;}
     bool FillCup();
private:
     bool is_Drop_Down = false;
     bool is_Right_Drop = true;
     bool is_Fail_Drop = false;
     int drop_down_step = 1;
     int tmp_fill_level = 0;
    int cup_fill_level = 0,character_step=3;
    QTimer drop_down_timer;
signals:

public slots:
    void Drop_step();
    void draw(class QPaintDevice *painter);
    void MoveRight();
    void MoveLeft();
};

#endif // CHARACTER_H
