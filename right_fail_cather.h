#ifndef RIGHT_FAIL_CATHER_H
#define RIGHT_FAIL_CATHER_H
#include "drawable.h"

class Right_Fail_Catcher : public Drawable
{
    Q_OBJECT
public:
    Right_Fail_Catcher(class GameState* _gs,QObject *parent = 0);
    void draw(QPaintDevice *painter)override;
    void Do_Wet();
private:
    int wet_step=1;
    bool is_wet = false;
    QTimer t;
    int step=1;
private slots:
    void Do_Step();
};

#endif // RIGHT_FAIL_CATHER_H
