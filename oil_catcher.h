#ifndef OIL_CATCHER_H
#define OIL_CATCHER_H
#include "drawable.h"

class Oil_Catcher : public Drawable
{
    Q_OBJECT
public:
    Oil_Catcher(GameState *_gs, QObject* parent = 0);
    void draw(class QPaintDevice *painter)override;
 int getStep(){return step;}
private:

    int step = 1;
    bool back = false;
    QTimer t;
private slots:
    void Do_Step();
};

#endif // OIL_CATCHER_H
